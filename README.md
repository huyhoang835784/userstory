# UserStory
## Title ##
Delete booked table
## Value Statement ##
As a user ,I want to delete the booked table so the restaurant doesn't need to prepare for me
## Acceptance Criteria ##
- When the user requests to delete the booked table
- Then ensure that table had been empty 
## Definition of Done ##
- Unit Test Passed
- Acceptance Criteria Met
- Code Reviewed
- Functional Tests Passed
- Non-Functional Requirements Met
- Product Owner Accepts User Story
## Owner ## 
MR / Owner


